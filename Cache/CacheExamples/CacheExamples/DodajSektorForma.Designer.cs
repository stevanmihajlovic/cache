﻿namespace CacheExamples
{
    partial class DodajSektorForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dodajSektorButton = new System.Windows.Forms.Button();
            this.nazivTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dodajSektorButton
            // 
            this.dodajSektorButton.Location = new System.Drawing.Point(92, 85);
            this.dodajSektorButton.Name = "dodajSektorButton";
            this.dodajSektorButton.Size = new System.Drawing.Size(109, 23);
            this.dodajSektorButton.TabIndex = 0;
            this.dodajSektorButton.Text = "Dodaj sektor";
            this.dodajSektorButton.UseVisualStyleBackColor = true;
            this.dodajSektorButton.Click += new System.EventHandler(this.dodajSektorButton_Click);
            // 
            // nazivTextBox
            // 
            this.nazivTextBox.Location = new System.Drawing.Point(70, 49);
            this.nazivTextBox.Name = "nazivTextBox";
            this.nazivTextBox.Size = new System.Drawing.Size(145, 20);
            this.nazivTextBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(89, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Unesite naziv sektora:";
            // 
            // DodajSektorForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 120);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.nazivTextBox);
            this.Controls.Add(this.dodajSektorButton);
            this.Name = "DodajSektorForma";
            this.Text = "DodajSektorForma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button dodajSektorButton;
        private System.Windows.Forms.TextBox nazivTextBox;
        private System.Windows.Forms.Label label1;
    }
}