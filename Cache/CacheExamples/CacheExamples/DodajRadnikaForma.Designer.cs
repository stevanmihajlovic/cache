﻿namespace CacheExamples
{
    partial class DodajRadnikaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.imeTextBox = new System.Windows.Forms.TextBox();
            this.prezimeTextBox = new System.Windows.Forms.TextBox();
            this.jmbgTextBox = new System.Windows.Forms.TextBox();
            this.plataTextBox = new System.Windows.Forms.TextBox();
            this.telefonTextBox = new System.Windows.Forms.TextBox();
            this.dodajRadnikaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ime:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Prezime:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 68);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "JMBG:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Plata:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Telefon:";
            // 
            // imeTextBox
            // 
            this.imeTextBox.Location = new System.Drawing.Point(93, 13);
            this.imeTextBox.Name = "imeTextBox";
            this.imeTextBox.Size = new System.Drawing.Size(140, 20);
            this.imeTextBox.TabIndex = 5;
            // 
            // prezimeTextBox
            // 
            this.prezimeTextBox.Location = new System.Drawing.Point(93, 39);
            this.prezimeTextBox.Name = "prezimeTextBox";
            this.prezimeTextBox.Size = new System.Drawing.Size(140, 20);
            this.prezimeTextBox.TabIndex = 6;
            // 
            // jmbgTextBox
            // 
            this.jmbgTextBox.Location = new System.Drawing.Point(93, 65);
            this.jmbgTextBox.Name = "jmbgTextBox";
            this.jmbgTextBox.Size = new System.Drawing.Size(140, 20);
            this.jmbgTextBox.TabIndex = 7;
            // 
            // plataTextBox
            // 
            this.plataTextBox.Location = new System.Drawing.Point(93, 91);
            this.plataTextBox.Name = "plataTextBox";
            this.plataTextBox.Size = new System.Drawing.Size(140, 20);
            this.plataTextBox.TabIndex = 8;
            // 
            // telefonTextBox
            // 
            this.telefonTextBox.Location = new System.Drawing.Point(93, 117);
            this.telefonTextBox.Name = "telefonTextBox";
            this.telefonTextBox.Size = new System.Drawing.Size(140, 20);
            this.telefonTextBox.TabIndex = 9;
            // 
            // dodajRadnikaButton
            // 
            this.dodajRadnikaButton.Location = new System.Drawing.Point(131, 165);
            this.dodajRadnikaButton.Name = "dodajRadnikaButton";
            this.dodajRadnikaButton.Size = new System.Drawing.Size(102, 23);
            this.dodajRadnikaButton.TabIndex = 10;
            this.dodajRadnikaButton.Text = "Dodaj Radnika";
            this.dodajRadnikaButton.UseVisualStyleBackColor = true;
            this.dodajRadnikaButton.Click += new System.EventHandler(this.dodajRadnikaButton_Click);
            // 
            // DodajRadnikaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.dodajRadnikaButton);
            this.Controls.Add(this.telefonTextBox);
            this.Controls.Add(this.plataTextBox);
            this.Controls.Add(this.jmbgTextBox);
            this.Controls.Add(this.prezimeTextBox);
            this.Controls.Add(this.imeTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DodajRadnikaForma";
            this.Text = "DodajRadnikaForma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox imeTextBox;
        private System.Windows.Forms.TextBox prezimeTextBox;
        private System.Windows.Forms.TextBox jmbgTextBox;
        private System.Windows.Forms.TextBox plataTextBox;
        private System.Windows.Forms.TextBox telefonTextBox;
        public System.Windows.Forms.Button dodajRadnikaButton;
    }
}