﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using InterSystems.Data.CacheClient;
using InterSystems.Data.CacheTypes;
using Fabrika;

namespace CacheExamples
{
    public partial class DodajRadnikaForma : Form
    {
        CacheConnection connection;

        Radnik radnik;

        public DodajRadnikaForma()
        {
            InitializeComponent();
        }

        public DodajRadnikaForma(CacheConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        public DodajRadnikaForma(CacheConnection connection, Radnik radnik)
        {
            InitializeComponent();
            this.connection = connection;
            this.radnik = radnik;
            imeTextBox.Text = radnik.ime;
            prezimeTextBox.Text = radnik.prezime;
            jmbgTextBox.Text = radnik.jmbg;
            plataTextBox.Text = radnik.plata.ToString();
            telefonTextBox.Text = radnik.telefon[0];
            dodajRadnikaButton.Text = "Updatuj radnika";
        }

        private void dodajRadnikaButton_Click(object sender, EventArgs e)
        {
            if (radnik == null)
            {
                Radnik r = new Radnik(connection);

                r.ime = imeTextBox.Text;

                r.prezime = prezimeTextBox.Text;

                r.jmbg = jmbgTextBox.Text;

                r.plata = Convert.ToInt32(plataTextBox.Text);

                r.telefon.Add(telefonTextBox.Text);

                CacheStatus status2 = r.Save();

                if (status2.IsOK)
                    MessageBox.Show("Uspesno ste dodali radnika u bazu");
                else
                    MessageBox.Show("Niste uspesno dodali radnika u bazu");

                r.Close();

                this.Close();
            }
            else
            {
                string query = "update fabrika.radnik set " +
                                "ime='" + imeTextBox.Text +
                                "', prezime='"+prezimeTextBox.Text+
                                "', jmbg='"+jmbgTextBox.Text+
                                "', plata='"+plataTextBox.Text+
                                "', telefon='"+telefonTextBox.Text+
                                "' where id = '" + radnik.Id()+"'";

                CacheCommand cmd = new CacheCommand(query, connection);

                cmd.ExecuteNonQuery();

                this.Close();
            }
        }
    }
}
