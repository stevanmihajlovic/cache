﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using InterSystems.Data.CacheClient;
using InterSystems.Data.CacheTypes;
using Fabrika;

namespace CacheExamples
{
    public partial class DodajSektorForma : Form
    {
        CacheConnection connection;

        Sektor sektor;

        public DodajSektorForma()
        {
            InitializeComponent();
        }

        public DodajSektorForma(CacheConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
        }

        public DodajSektorForma(CacheConnection connection,Sektor sektor)
        {
            InitializeComponent();
            this.connection = connection;
            this.sektor = sektor;
            dodajSektorButton.Text = "Updatuj sektor";
            nazivTextBox.Text = sektor.naziv;
        }

        private void dodajSektorButton_Click(object sender, EventArgs e)
        {
            if (sektor == null)
            {
                Sektor s = new Sektor(connection);

                s.naziv = nazivTextBox.Text;

                CacheStatus status1 = s.Save();

                if (status1.IsOK)
                    MessageBox.Show("Uspesno ste dodali sektor u bazu");
                else
                    MessageBox.Show("Niste uspesno dodali sektor u bazu");

                s.Close();

                this.Close();
            }
            else
            {
                string query = "update fabrika.sektor set " +
                                "naziv= '"+nazivTextBox.Text+
                                "' where id = '"+sektor.Id()+"'";

                CacheCommand cmd = new CacheCommand(query, connection);

                cmd.ExecuteNonQuery();

                this.Close();
            }
        }
    }
}
