﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using InterSystems.Data.CacheClient;
using InterSystems.Data.CacheTypes;
using Fabrika;

namespace CacheExamples
{
    public partial class PostaviSefaForma : Form
    {
        CacheConnection connection;

        public PostaviSefaForma()
        {
            InitializeComponent();
        }

        public PostaviSefaForma(CacheConnection connection)
        {
            InitializeComponent();
            this.connection = connection;
            PopuniListView();
        }

        private void PopuniListView()
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count==1 && listView2.SelectedIndices.Count==1)
            {
                Radnik r = Fabrika.Radnik.OpenId(connection, listView1.SelectedItems[0].SubItems[0].Text);
                Sektor s = Sektor.OpenId(connection, listView2.SelectedItems[0].SubItems[0].Text);                             

                //provera da li sektor ima odredjenog sefa
                string sql = "select id from fabrika.sefradnik where sektor = ?";
                CacheCommand cmd = new CacheCommand(sql,connection);
                CacheParameter param = new CacheParameter();
                param.CacheDbType = CacheDbType.NVarChar;
                param.Value = s.Id();
                cmd.Parameters.Add(param);
                object ob = cmd.ExecuteScalar();

                if (ob != null)    //provera da li sektor vec ima sefa
                {
                    StandardnRadnik radnik = new StandardnRadnik(connection);
                    SefRadnik stariSef = SefRadnik.OpenId(connection, ob.ToString());
                    radnik.ime = stariSef.ime;
                    radnik.prezime = stariSef.prezime;
                    radnik.jmbg = stariSef.jmbg;
                    radnik.plata = stariSef.plata;
                    radnik.telefon = stariSef.telefon;
                    radnik.radiU = s;
                    radnik.Save();
                   
                    SefRadnik.DeleteId(connection, stariSef.Id());
                }
                s = Sektor.OpenId(connection, listView2.SelectedItems[0].SubItems[0].Text);                             
                SefRadnik noviSef = s.postaviSefa(r);
                noviSef.Save();
                MessageBox.Show("Uspesno ste postavili sefa sektora");
                s.Save();
                this.Close();
            }
        }

        private void PostaviSefaForma_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView2.View = View.Details;
            listView2.GridLines = true;
            listView1.FullRowSelect = true;
            listView2.FullRowSelect = true;
            listView1.MultiSelect = true;
            listView2.MultiSelect = true;

            //popunjavamo listview za radnike
            string sql = "select id from Fabrika.Radnik";
            CacheCommand cmd = new CacheCommand(sql, connection);


            CacheDataReader reader = cmd.ExecuteReader();
            int cols = 7;

            listView1.Clear();
            listView1.Columns.Add("ID", listView1.Width / cols);
            listView1.Columns.Add("Ime", listView1.Width / cols);
            listView1.Columns.Add("Prezime", listView1.Width / cols);
            listView1.Columns.Add("Jmbg", listView1.Width / cols);
            listView1.Columns.Add("Plata", listView1.Width / cols);
            listView1.Columns.Add("Telefon", listView1.Width / cols);
            listView1.Columns.Add("Status", listView1.Width / cols);


            while (reader.Read())
            {
                int id = (int)reader[reader.GetOrdinal("id")];
                //string ime = (string)reader[reader.GetOrdinal("ime")];
                //string prezime = (string)reader[reader.GetOrdinal("prezime")];
                //string jmbg = (string)reader[reader.GetOrdinal("jmbg")];
                //int plata = (int)reader[reader.GetOrdinal("plata")];
                //string telefon = (string)reader[reader.GetOrdinal("telefon")];
                //listView1.Items.Add(new ListViewItem(new string[] { id.ToString(), ime, prezime, jmbg, plata.ToString(), telefon }));
                Radnik r = Radnik.OpenId(connection, id.ToString());

                
                listView1.Items.Add(new ListViewItem(new string[] { id.ToString(), r.ime, r.prezime, r.jmbg, r.plata.ToString(), r.telefon[0].ToString(),r.informacijeORadniku()}));
            }

            //popunjavamo listview za sektore
            sql = "select * from Fabrika.Sektor";
            
            cmd = new CacheCommand(sql, connection);


            reader = cmd.ExecuteReader();
            cols = reader.FieldCount;


            listView2.Clear();
            listView2.Columns.Add("ID", listView2.Width / cols);
            listView2.Columns.Add("Naziv sektora", listView2.Width / cols);



            while (reader.Read())
            {
                int id = (int)reader[reader.GetOrdinal("id")];
                string naziv = (string)reader[reader.GetOrdinal("naziv")];

                listView2.Items.Add(new ListViewItem(new string[] { id.ToString(), naziv }));
            }
            reader.Close();

        }

        private void zaposliRadnikaButton_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 1 && listView2.SelectedIndices.Count == 1)
            {
                Radnik r = Fabrika.Radnik.OpenId(connection, listView1.SelectedItems[0].SubItems[0].Text);
                Sektor s = Sektor.OpenId(connection, listView2.SelectedItems[0].SubItems[0].Text);

                StandardnRadnik radnik = new StandardnRadnik(connection);
                radnik.ime = r.ime;
                radnik.prezime = r.prezime;
                radnik.jmbg = r.jmbg;
                radnik.plata = r.plata;
                radnik.telefon = r.telefon;
                radnik.radiU = s;
                radnik.Save();
                Radnik.DeleteId(connection, r.Id());
                MessageBox.Show("Uspesno ste zaposlili radnika");
                s.radnici.Add(radnik);
                s.Save();
                this.Close();
            }
        }
    }
}
