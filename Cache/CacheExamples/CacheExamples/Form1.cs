﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using InterSystems.Data.CacheClient;
using InterSystems.Data.CacheTypes;
using Fabrika;

namespace CacheExamples
{
    public partial class Form1 : Form
    {
        CacheConnection connection;

        string tablename;

        public void CreateConnection()
        {          
            try
            {

                connection = new CacheConnection();
                connection.ConnectionString = CacheConnection.ConnectDlg();     
                connection.Open();

            }
            catch (Exception e)
            {               
                MessageBox.Show(e.Message);
                if (connection != null)
                    connection.Close();
            }
        }



        public Form1()
        {
            InitializeComponent();
            CreateConnection();
        }

        public void DodajSektor ()  
        {           
            Sektor s = new Sektor(connection);

            s.naziv = "Marketing";

            CacheStatus status1 = s.Save();     
    
            MessageBox.Show(status1.IsOK.ToString());   
       
            s.Close();           
        }
        public void DodajRadnika () 
        {
            Radnik r = new StandardnRadnik(connection);

            r.ime = "Mika";

            r.prezime = "Mikic";

            r.jmbg = "55555";

            r.plata = 10000;

            r.telefon.Add("4321");

            r.telefon.Add("01234");

            CacheStatus status2 = r.Save();

            MessageBox.Show(status2.IsOK.ToString());

            r.Close();
        }

        public void UcitajSektor ()
        {
            string id = "4";
            Sektor s = Sektor.OpenId(connection, id);
            if (s != null)
            {
                MessageBox.Show(s.naziv+""+s.sef);
            }
            else
            {
                MessageBox.Show("Nijedan sektor nije pronadjen.");
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DodajSektorForma form = new DodajSektorForma(this.connection);
            form.ShowDialog();
            listView1.Clear();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            UcitajSektor();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DodajRadnikaForma form = new DodajRadnikaForma(this.connection);
            form.ShowDialog();
            listView1.Clear();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            PostaviSefaForma form = new PostaviSefaForma(this.connection);
            form.ShowDialog();
            listView1.Clear();   
        }

        private void button5_Click(object sender, EventArgs e)
        {
            String query = "SELECT * from fabrika.radnik where plata > 5000";

            CacheCommand cmd = new CacheCommand(query, connection);

            CacheDataReader reader = cmd.ExecuteReader();

            while (reader.Read ())
            {
                
                string ime = (string)reader[reader.GetOrdinal ("ime")];
                string prezime = (string)reader[reader.GetOrdinal ("prezime")];
                string jmbg = (string)reader[reader.GetOrdinal ("jmbg")];

                MessageBox.Show(ime + " " + prezime + " " + jmbg);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int brSektora = 4;
            string query = "select * from fabrika.standardnradnik where radiU > ?";

            CacheParameter brojSektora = new CacheParameter();
            brojSektora.CacheDbType = CacheDbType.Int;
            brojSektora.Direction = ParameterDirection.Input;
            brojSektora.Value = brSektora;

            CacheCommand cmd = new CacheCommand(query, connection);
            cmd.Parameters.Add(brojSektora);

            CacheDataReader reader = cmd.ExecuteReader();

            while (reader.Read ())
            {
                string ime = (string)reader[reader.GetOrdinal ("ime")];
                string prezime = (string)reader[reader.GetOrdinal ("prezime")];
                string jmbg = (string)reader[reader.GetOrdinal ("jmbg")];

                MessageBox.Show(ime + " " + prezime + " " + jmbg);
            }
           
        }

        private void button7_Click(object sender, EventArgs e)
        {
            CacheTransaction trans = connection.BeginTransaction(IsolationLevel.ReadCommitted);
            try
            {
                string sql = "insert into fabrika.standardnradnik (ime, prezime, jmbg, plata) values (?,?,?,?)";
                CacheCommand cmd = new CacheCommand(sql, connection);

                CacheParameter param1 = new CacheParameter();
                param1.CacheDbType = CacheDbType.NVarChar;
                param1.Value = "Nikola";

                CacheParameter param2 = new CacheParameter();
                param2.CacheDbType = CacheDbType.NVarChar;
                param2.Value = "Nikolic";

                CacheParameter param3 = new CacheParameter();
                param3.CacheDbType = CacheDbType.NVarChar;
                param3.Value = "1234";

                CacheParameter param4 = new CacheParameter();
                param4.CacheDbType = CacheDbType.Int;
                param4.Value = "15000";

                cmd.Parameters.Add (param1);
                cmd.Parameters.Add(param2);
                cmd.Parameters.Add(param3);
                cmd.Parameters.Add(param4);

                cmd.ExecuteNonQuery();        
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
                trans.Rollback();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //brisanje objekata korišćenjem oid-a (brisanje sektora 5)
            string id = "5";
            CacheStatus sc = Fabrika.Sektor.DeleteId(connection, id);
            MessageBox.Show("Deleting status: " + sc.IsOK.ToString());
            MessageBox.Show("Radnik sa id-em:" + id + Fabrika.Radnik.ExistsId(connection, id).ToString());

            //brisanje korišćenjem SQL-a (brisanje svih radnika u sektoru 5)
            string query = "delete from fabrika.standardnradnik where radiU = ?";

            CacheParameter brojSektora = new CacheParameter();
            brojSektora.CacheDbType = CacheDbType.Int;
            brojSektora.Direction = ParameterDirection.Input;
            brojSektora.Value = 5;

            CacheCommand cmd = new CacheCommand(query, connection);
            cmd.Parameters.Add(brojSektora);

            cmd.ExecuteNonQuery();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            string sektori = "insert into fabrika.sektor(naziv) values ('Prodaja');"+
                              "insert into fabrika.sektor(naziv) values ('Marketing')";

            string radnici = "insert into fabrika.standardnradnik (ime,prezime,jmbg,telefon,plata) values ('Aleksa','Ravnihar','1111','1234',10000);" +
                            "insert into fabrika.standardnradnik (ime,prezime,jmbg,telefon,plata) values ('Stefan','Stojiljkovic','2222','1243',10000);" +
                            "insert into fabrika.standardnradnik (ime,prezime,jmbg,telefon,plata) values ('Stevan','Mihajlovic','3333','2134',10000);" +
                            "insert into fabrika.standardnradnik (ime,prezime,jmbg,telefon,plata) values ('Dusan','Djordjevic','4444','1324',10000)";
            CacheCommand cmd1 = new CacheCommand(sektori, connection);
            int rows1 = cmd1.ExecuteNonQuery();
            MessageBox.Show("Uspesno dodati sektori \"Prodaja\" i \"Marketing\" u bazu podataka.");
            CacheCommand cmd2 = new CacheCommand(radnici, connection);
            int rows2 = cmd2.ExecuteNonQuery();
            MessageBox.Show("Ukupno dodato " + rows2.ToString() + " radnika");

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listView1.View = View.Details;
            listView1.GridLines = true;
            listView1.FullRowSelect = true;
        }

        private void button10_Click(object sender, EventArgs e)
        {          
            string sql = "select * from Fabrika.Radnik";
            CacheCommand cmd = new CacheCommand(sql, connection);
            

            CacheDataReader reader = cmd.ExecuteReader();
            int cols = reader.FieldCount;

            listView1.Clear();
            listView1.Columns.Add("ID",listView1.Width/cols);
            listView1.Columns.Add("Ime", listView1.Width / cols);
            listView1.Columns.Add("Prezime", listView1.Width / cols);
            listView1.Columns.Add("Jmbg", listView1.Width / cols);
            listView1.Columns.Add("Plata", listView1.Width / cols);
            listView1.Columns.Add("Telefon", listView1.Width / cols);


            while (reader.Read())
            {
                int id = (int)reader[reader.GetOrdinal("id")];
                string ime = (string)reader[reader.GetOrdinal("ime")];
                string prezime = (string)reader[reader.GetOrdinal("prezime")];               
                string jmbg = (string)reader[reader.GetOrdinal("jmbg")];
                int plata = (int)reader[reader.GetOrdinal("plata")];
                string telefon = (string)reader[reader.GetOrdinal("telefon")];
                listView1.Items.Add(new ListViewItem(new string[] { id.ToString(), ime, prezime,jmbg, plata.ToString(), telefon }));
            }

            tablename = "fabrika.radnik";
        }

        private void button11_Click(object sender, EventArgs e)
        {
            string sql = "select * from Fabrika.Sektor";
            CacheCommand cmd = new CacheCommand(sql, connection);


            CacheDataReader reader = cmd.ExecuteReader();
            int cols = reader.FieldCount;


            listView1.Clear();
            listView1.Columns.Add("ID", listView1.Width / cols);
            listView1.Columns.Add("Naziv sektora", listView1.Width / cols);



            while (reader.Read())
            {
                int id = (int)reader[reader.GetOrdinal("id")];
                string naziv = (string)reader[reader.GetOrdinal("naziv")];
            
                listView1.Items.Add(new ListViewItem(new string[] { id.ToString(), naziv }));
            }

            tablename = "fabrika.sektor";
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string sql = "select * from Fabrika.SefRadnik";
            CacheCommand cmd = new CacheCommand(sql, connection);


            CacheDataReader reader = cmd.ExecuteReader();
            int cols = reader.FieldCount;

            listView1.Clear();
            listView1.Columns.Add("ID", listView1.Width / cols);
            listView1.Columns.Add("Ime", listView1.Width / cols);
            listView1.Columns.Add("Prezime", listView1.Width / cols);
            listView1.Columns.Add("Jmbg", listView1.Width / cols);
            listView1.Columns.Add("Plata", listView1.Width / cols);
            listView1.Columns.Add("Telefon", listView1.Width / cols);
            listView1.Columns.Add("Sektor", listView1.Width / cols);


            while (reader.Read())
            {
                int id = (int)reader[reader.GetOrdinal("id")];
                string ime = (string)reader[reader.GetOrdinal("ime")];
                string prezime = (string)reader[reader.GetOrdinal("prezime")];
                string jmbg = (string)reader[reader.GetOrdinal("jmbg")];
                int plata = (int)reader[reader.GetOrdinal("plata")];
                string telefon = (string)reader[reader.GetOrdinal("telefon")];
                int sektor = (int)reader[reader.GetOrdinal("sektor")];
                listView1.Items.Add(new ListViewItem(new string[] { id.ToString(), ime, prezime, jmbg, plata.ToString(), telefon, sektor.ToString() }));
            }

            tablename = "fabrika.radnik";
        }

        private void button13_Click(object sender, EventArgs e)
        {

        }

        private void button13_Click_1(object sender, EventArgs e)
        {
            string sql = "delete from fabrika.radnik;delete from fabrika.sektor;";
            CacheCommand cmd = new CacheCommand(sql, connection);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Obrisano.");
        }



        private void izbrisiSelektovanuStavkuButton_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 1)
            {
                string query;
                if (tablename.Equals("fabrika.sektor"))
                {
                    query = "delete from fabrika.standardnradnik where radiU = ?;delete from fabrika.sefradnik where sektor = ?";

                    CacheParameter brojSektora = new CacheParameter();
                    brojSektora.CacheDbType = CacheDbType.Int;
                    brojSektora.Direction = ParameterDirection.Input;
                    brojSektora.Value = Convert.ToInt32(listView1.SelectedItems[0].SubItems[0].Text);

                    CacheParameter brojSektora1 = new CacheParameter();
                    brojSektora1.CacheDbType = CacheDbType.Int;
                    brojSektora1.Direction = ParameterDirection.Input;
                    brojSektora1.Value = Convert.ToInt32(listView1.SelectedItems[0].SubItems[0].Text);

                    CacheCommand cmd = new CacheCommand(query, connection);
                    cmd.Parameters.Add(brojSektora);
                    cmd.Parameters.Add(brojSektora1);

                    cmd.ExecuteNonQuery();
                }
                query = "delete from " + tablename + " where id = ?";    //kako izbrisati veze

                CacheParameter id = new CacheParameter();
                id.CacheDbType = CacheDbType.Int;
                id.Direction = ParameterDirection.Input;
                id.Value = Convert.ToInt32(listView1.SelectedItems[0].SubItems[0].Text);

                CacheCommand cmd1 = new CacheCommand(query, connection);
                cmd1.Parameters.Add(id);

                cmd1.ExecuteNonQuery();
                listView1.Items.Remove(listView1.SelectedItems[0]);
            }
        }

        private void updatujSelektovanuStavkuButton_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedIndices.Count == 1)
            {
                if (tablename.Equals("fabrika.sektor"))
                {
                    Sektor s = Sektor.OpenId(connection, listView1.SelectedItems[0].SubItems[0].Text);   
                    DodajSektorForma form = new DodajSektorForma(this.connection,s);
                    form.ShowDialog();
                }
                else
                {
                    Radnik r = Fabrika.Radnik.OpenId(connection, listView1.SelectedItems[0].SubItems[0].Text);
                    DodajRadnikaForma form = new DodajRadnikaForma(this.connection,r);
                    form.ShowDialog();
                }
                listView1.Clear();
            }
        }
    }
}
