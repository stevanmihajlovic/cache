﻿namespace CacheExamples
{
    partial class PostaviSefaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.postaviSefaButton = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.listView2 = new System.Windows.Forms.ListView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.zaposliRadnikaButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // postaviSefaButton
            // 
            this.postaviSefaButton.Location = new System.Drawing.Point(359, 331);
            this.postaviSefaButton.Name = "postaviSefaButton";
            this.postaviSefaButton.Size = new System.Drawing.Size(147, 39);
            this.postaviSefaButton.TabIndex = 4;
            this.postaviSefaButton.Text = "Postavite izabranog radnika za sefa izabranog sektora";
            this.postaviSefaButton.UseVisualStyleBackColor = true;
            this.postaviSefaButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(47, 19);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(381, 127);
            this.listView1.TabIndex = 10;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // listView2
            // 
            this.listView2.Location = new System.Drawing.Point(47, 19);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(381, 111);
            this.listView2.TabIndex = 11;
            this.listView2.UseCompatibleStateImageBehavior = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(491, 156);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Izaberite radnika:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listView2);
            this.groupBox2.Location = new System.Drawing.Point(15, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(491, 136);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Izaberite sektor:";
            // 
            // zaposliRadnikaButton
            // 
            this.zaposliRadnikaButton.Location = new System.Drawing.Point(216, 331);
            this.zaposliRadnikaButton.Name = "zaposliRadnikaButton";
            this.zaposliRadnikaButton.Size = new System.Drawing.Size(137, 39);
            this.zaposliRadnikaButton.TabIndex = 14;
            this.zaposliRadnikaButton.Text = "Zaposli izabranog radnika u izabrani sektor";
            this.zaposliRadnikaButton.UseVisualStyleBackColor = true;
            this.zaposliRadnikaButton.Click += new System.EventHandler(this.zaposliRadnikaButton_Click);
            // 
            // PostaviSefaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 381);
            this.Controls.Add(this.zaposliRadnikaButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.postaviSefaButton);
            this.Name = "PostaviSefaForma";
            this.Text = "PostaviSefaForma";
            this.Load += new System.EventHandler(this.PostaviSefaForma_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button postaviSefaButton;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button zaposliRadnikaButton;
    }
}