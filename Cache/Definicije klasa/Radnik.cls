Class Fabrika.Radnik Extends %Persistent
{

Property ime As %String;

Property prezime As %String;

Property jmbg As %String [ Required ];

Property plata As %Integer;

Property telefon As list Of %String;

Index imeIndex On ime;

Index prezimeIndex On prezime;

Storage Default
{
<Data name="RadnikDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>ime</Value>
</Value>
<Value name="3">
<Value>prezime</Value>
</Value>
<Value name="4">
<Value>radiU</Value>
</Value>
<Value name="5">
<Value>jmbg</Value>
</Value>
<Value name="6">
<Value>plata</Value>
</Value>
<Value name="7">
<Value>telefon</Value>
</Value>
</Data>
<DataLocation>^Fabrika.RadnikD</DataLocation>
<DefaultData>RadnikDefaultData</DefaultData>
<IdLocation>^Fabrika.RadnikD</IdLocation>
<IndexLocation>^Fabrika.RadnikI</IndexLocation>
<StreamLocation>^Fabrika.RadnikS</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

Index jmbgIndex On jmbg [ Unique ];

Method informacijeORadniku() As %String
{
	quit "Nerasporedjen u sektor."
}

Query vratiRadnike(plata As %Integer) As %SQLQuery
{
	SELECT * FROM FABRIKA.RADNIk where plata>:plata
}

Method povecajPlatu(plata As %Integer)
{
	Set ..plata = ..plata+plata
}

}
