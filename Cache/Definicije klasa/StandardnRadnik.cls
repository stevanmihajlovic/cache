Class Fabrika.StandardnRadnik Extends Fabrika.Radnik
{

Relationship radiU As Fabrika.Sektor [ Cardinality = one, Inverse = radnici ];

Storage Default
{
<Type>%Library.CacheStorage</Type>
}

Method informacijeORadniku() As %String
{
	Quit "Radnik sektora: " _ ..radiU.naziv
}

}

