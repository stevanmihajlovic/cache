Class Fabrika.Sektor Extends %Persistent
{

Parameter CLASSINFO As %String = "Ovo je klasa koja modeluje sektor jedne fabrike.";

Parameter BONUSPLATA As %Integer = 20000;

Property naziv As %String;

Relationship sef As Fabrika.SefRadnik [ Cardinality = many, Inverse = sektor ];

Relationship radnici As Fabrika.StandardnRadnik [ Cardinality = many, Inverse = radiU ];

Method idGet() As %String [ ServerOnly = 1 ]
{
		Quit ""
}

Method idSet(Arg As %String) As %Status [ ServerOnly = 1 ]
{
		Quit $$$OK
}

Storage Default
{
<Data name="SektorDefaultData">
<Value name="1">
<Value>%%CLASSNAME</Value>
</Value>
<Value name="2">
<Value>id</Value>
</Value>
<Value name="3">
<Value>naziv</Value>
</Value>
<Value name="4">
<Value>sefSektora</Value>
</Value>
</Data>
<DataLocation>^Fabrika.SektorD</DataLocation>
<DefaultData>SektorDefaultData</DefaultData>
<IdLocation>^Fabrika.SektorD</IdLocation>
<IndexLocation>^Fabrika.SektorI</IndexLocation>
<StreamLocation>^Fabrika.SektorS</StreamLocation>
<Type>%Library.CacheStorage</Type>
}

Method izmeniNazivSektora(naziv As %String) [ Final ]
{

		set ..naziv=naziv
}

Method postaviSefa(noviSef As Radnik) As Fabrika.SefRadnik [ Final ]
{
		
		set id = noviSef.%Id()
		
		Set nSef = ##class(Fabrika.SefRadnik).%New()
		set nSef.ime = noviSef.ime
		set nSef.prezime = noviSef.prezime
		set nSef.jmbg = noviSef.jmbg
		set nSef.telefon = noviSef.telefon
		set nSef.plata = noviSef.plata
		set naz = ..naziv
		
		&sql(select id into :thisID from fabrika.sektor where naziv=:naz)
		
		set nSef.sektor = ##class(Fabrika.Sektor).%OpenId(thisID)
		do nSef.povecajPlatu(..#BONUSPLATA)	
		
		set ..sef = nSef
		set stat =  nSef.%SaveDirect()
		do ##class(Fabrika.Radnik).%DeleteId(id)
		Quit nSef
}

ClassMethod PodaciOKlasi() As %String
{
		quit ..#CLASSINFO
}

}
