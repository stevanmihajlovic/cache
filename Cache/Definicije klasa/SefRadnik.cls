Class Fabrika.SefRadnik Extends Fabrika.Radnik
{

Relationship sektor As Fabrika.Sektor [ Cardinality = one, Inverse = sef ];

Storage Default
{
<Data name="SefRadnikDefaultData">
<Subscript>"SefRadnik"</Subscript>
<Value name="1">
<Value>sektor</Value>
</Value>
</Data>
<DefaultData>SefRadnikDefaultData</DefaultData>
<Type>%Library.CacheStorage</Type>
}

Method informacijeORadniku() As %String
{
	quit "Sef sektora: " _ ..sektor.naziv
}

}
